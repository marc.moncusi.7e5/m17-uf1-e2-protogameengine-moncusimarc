# M17-UF1-E2- Proto Game Engine

## Repte 1

Hem d'imprimir una matriu amb 0's. L'animació recrearà la pluja de caràcters de la pel·lícula Matrix. Una lletra aleatòria caurà en cada frame una fila fins a arribar al final. La columna on es generi també serà aleatori.

## Repte 2

Les lletres cauran per la columna que l'usuari ha seleccionat. L'usuari pot indicar la columna de sortida amb el número de columna o amb un cursor mogut per fletxes.
