﻿using MyFirstProgram;
using System;
using System.Collections.Generic;
using System.Text;
using GameTools;

/// <summary>
/// Author:         Marc Moncusí i Rubio
/// Description:    Simulació de pluja de lletres estil matrix
/// Date:           23/09/2022
/// </summary>
namespace M17_UF1_E2_ProtoGameEngine
{
    /// <summary>
    /// Classe amb herencia de GameEngine
    /// </summary>
    class MyGameAnimation : GameEngine
    {
        private MatrixRepresentation matrix;
        private char[,] MyMatrix;
        private readonly int width = 20;
        private readonly int height = 20;

        public MyGameAnimation() : base() { }

        /// <summary>
        /// Inicialització de l'array multidimensional
        /// </summary>
        protected override void Start()
        {
            matrix = new MatrixRepresentation(width, height);
            matrix.CleanTheMatrix();
            MyMatrix = matrix.TheMatrix;
            matrix.printMatrix();
        }

        /// <summary>
        /// Inicialització, actualització e impresió de les gotes
        /// </summary>
        protected override void Update()
        {
            UpdateDrops();
            InitDrops();
            PrintDrops();
        }

        /// <summary>
        /// Inicialització de les gotes de forma aleatoria
        /// Valors 49 a 53, determinen els valors ascii dels numeros del 1  al 5
        /// </summary>
        private void InitDrops()
        {
            Random rand = new Random();
            for (var i = 0; i < MyMatrix.GetLength(0); i++)
            {
                if (rand.Next(0, 10) == 0)
                {
                    MyMatrix[i, 0] = (char)rand.Next(49, 53);
                }
            }
        }
        /// <summary>
        /// Actualització de les gotes ja presents al array
        /// </summary>
        private void UpdateDrops()
        {
            for (var i = 0; i < MyMatrix.GetLength(0); i++)
            {
                for (var j = MyMatrix.GetLength(1) - 1; j >= 1; j--)
                {
                    if (MyMatrix[i, j] == '0')
                    {
                        MyMatrix[i, j] = MyMatrix[i, j - 1];
                    }
                    else
                    {
                        MyMatrix[i, j]--;
                    }

                }
            }
            for (var i = 0; i < MyMatrix.GetLength(0); i++)
            {
                if (MyMatrix[i, 0] != '0')
                {
                    MyMatrix[i, 0]--;
                }
            }
        }
        /// <summary>
        /// Impresió de les lletres amb un color de fons
        /// Valors 65 a 90, determinen els valors ascii de les lletres en majuscula
        /// </summary>
        private void PrintDrops()
        {
            Random rand = new Random();
            Console.Clear();
            for (var y = 0; y < MyMatrix.GetLength(1); y++)
            {
                for (var x = 0; x < MyMatrix.GetLength(0); x++)
                {
                    Console.ForegroundColor = ColorChange(MyMatrix[x, y]);
                    if (MyMatrix[x, y] == '0')
                    {
                        Console.Write($" ");
                    }
                    else
                    {
                        Console.Write($"{(char)rand.Next(65, 90)}");
                    }
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// Funció amb retorn de consoleColor segons el valor de la gota en l'array
        /// </summary>
        /// <param name="value">Valor ascii de la gota</param>
        /// <returns></returns>
        private ConsoleColor ColorChange(char value)
        {
            ConsoleColor color;
            switch (value)
            {
                case '1':
                    color = ConsoleColor.White;
                    break;
                case '2':
                case '3':
                case '4':
                    color = ConsoleColor.Green;
                    break;
                case '5':
                    color = ConsoleColor.DarkGreen;
                    break;
                case '0':
                default:
                    color = ConsoleColor.Black;
                    break;
            }
            return color;
        }
        protected override void Exit()
        {

        }
    }
}